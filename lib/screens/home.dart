import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import 'myprofile.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(
          MediaQuery.of(context).size.width,
          MediaQuery.of(context).size.height,
        ),
        context: context,
        minTextAdapt: true,
        orientation: Orientation.portrait);
    return Scaffold(
      backgroundColor: const Color(0xffFBF9F7),
      appBar: AppBar(
        backgroundColor: const Color(0xff1D3149),
        leading: Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            top: 12,
            bottom: 12,
          ),
          child: Center(
            child: Image.asset(
              "assets/images/Vector_Smart_Object_xA0_Image.png",
              fit: BoxFit.cover,
            ),
          ),
        ),
        title: const Padding(
          padding: EdgeInsets.only(bottom: 24.5, top: 24.5),
          child: Center(
            child: Center(
              child: Text(
                "MARCH 31, 2022",
                style: TextStyle(
                    letterSpacing: 0.15,
                    fontFamily: "inter",
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    color: Color(0xffFFFFFF),
                    fontStyle: FontStyle.normal),
              ),
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: SvgPicture.asset(
              "assets/images/Notifications.svg",
              fit: BoxFit.contain,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 18.0),
            child:InkWell(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const MyProfile()));
              },
              child: SvgPicture.asset(
                "assets/images/Hamburger.svg",
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(30, 40, 16, 20),
        child: SizedBox(
          height: 800.h,
          width: 328.w,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                             // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/Tasks (4).svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("Tasks",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("7 Store Calls Planned",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              SizedBox(width: 55,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )
/*
                          Stack(
                          children: [
                            Positioned(
                              left: 5,
                              top: 14,
                              bottom: 12,
                              right: 284,
                              child: SizedBox(
                                  height: 24.h,
                                  width: 24.w,
                                  child: SvgPicture.asset(
                                    "assets/images/Tasks (4).svg",
                                  )),
                            ),
                            Positioned(
                              left: 64,
                              right: 190.28,
                              top: 12,
                              bottom: 27,
                              child: SizedBox(
                                height: 22.h,
                                width: 74.w,
                                child: const Text("Tasks",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff1D3149),
                                        fontFamily: 'inter')),
                              ),
                            ),
                            Positioned(
                              left: 64,
                              right: 33,
                              top: 34,
                              child: SizedBox(
                                height: 17.h,
                                width: 231.w,
                                child: const Text("7 Store Calls Planned",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff9B9EAD),
                                        fontFamily: 'inter')),
                              ),
                            ),
                            Positioned(
                              right: 40,
                              bottom: 35,
                              left: 300,
                              child: SizedBox(
                                  width: 7.w,
                                  height: 14.h,
                                  child: IconButton(
                                      onPressed: () {},
                                      icon: const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        color: Color(0xff737372),
                                      )
                                      )
                                      ),
                            )
                          ],
                        ),
*/
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/book-open.svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("Catalogues",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("Maecenas euneque porttitor",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              SizedBox(width: 10,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )

                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/file-minus.svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("Orders",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("Maecenas euneque porttitor",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              SizedBox(width: 10,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )

                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/shopping-bag (1).svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("In Store Process",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("Maecenas euneque porttitor",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              SizedBox(width: 10,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/message-square.svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("Messages",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("Maecenas euneque porttitor",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              const SizedBox(width: 10,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )

                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 18),
                  child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 2,
                      shadowColor: const Color(0xff14203E),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          height: 65.h,
                          width: 328.w,
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // SizedBox(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(20.0,16,20,16),
                                child: SvgPicture.asset(
                                  "assets/images/clock.svg",
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(top:12.0),
                                    child: Text("Clock-In/Out",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xff1D3149),
                                            fontFamily: 'inter')),
                                  ),
                                  SizedBox(
                                    height:3,
                                  ),
                                  Text("Maecenas euneque porttitor",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff9B9EAD),
                                          fontFamily: 'inter')),
                                ],
                              ),
                              SizedBox(width: 10,),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.arrow_forward_ios_outlined,
                                    color: Color(0xff737372),
                                  ))
                            ],
                          )

                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
