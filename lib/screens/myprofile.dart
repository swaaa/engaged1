import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: const Size(500,
        900),
        context: context,
        minTextAdapt: true,
        orientation: Orientation.portrait);
    return Scaffold(
      backgroundColor: const Color(0xffFBF9F7),
      appBar: AppBar(
        backgroundColor: const Color(0xff1D3149),
        leading: Padding(
          padding: const EdgeInsets.only(
            left: 10.0,
            top: 10,
            bottom: 8,
          ),
          child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Center(
                child: SvgPicture.asset(
                  "assets/images/arrow-left.svg",
                  fit: BoxFit.cover,
                ),
              )),
        ),
        title: const Padding(
          padding: EdgeInsets.only(bottom: 24.5, top: 24.5),
          child: Center(
            child: Center(
              child: Text(
                "My Profile",
                style: TextStyle(
                    letterSpacing: 0.15,
                    fontFamily: "inter",
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Color(0xffFFFFFF),
                    fontStyle: FontStyle.normal),
              ),
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: SvgPicture.asset(
              "assets/images/Notifications.svg",
              fit: BoxFit.contain,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 18.0),
            child: SvgPicture.asset(
              "assets/images/Hamburger.svg",
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(134, 24.0, 134, 12),
            child: SizedBox(
              height: 96.0.h,
              width: 96.0.w,
              child: Image.asset('assets/images/Rectangle 5.png'),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0, left: 107, right: 107),
            child: Text(
              "John Doe",
              style: TextStyle(
                  fontFamily: 'inter',
                  fontSize: 32,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff1D3149)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 135, right: 128),
            child: const Text(
              "@johndoe123",
              style: TextStyle(
                  fontFamily: 'inter',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff8697AC)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(125.0, 12, 97, 19),
            child: Row(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 2.0, top: 4, bottom: 4),
                  child: SizedBox(
                      width: 20.w,
                      height: 16.h,
                      child: SvgPicture.asset(
                        "assets/images/mail.svg",
                        fit: BoxFit.cover,
                      ) /*const Icon(
                      Icons.mail_outline,
                      color: Color(0xff8697AC),
                    ),*/
                      ),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 10.0, bottom: 4),
                  child: Text(
                    "johndoe@gmail.com",
                    style: TextStyle(
                        fontFamily: 'inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff403E3D)),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 60.0,
              right: 10,
            ),
            child: Row(
              children: [
                SizedBox(
                    height: 22.h,
                    width: 19.w,
                    child: SvgPicture.asset(
                      "assets/images/map-pin.svg",
                    )),
                const Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text(
                    "57 Loop St, Kaapstad, Western Cape 7420",
                    style: TextStyle(
                        fontFamily: 'inter',
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff403E3D)),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 28.0, left: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  shadowColor: Color(0xff1D3149),
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24.0),
                  ),
                  child: Container(
                    height: 108.h,
                    width: 108.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0xffFFFFF)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "10",
                          style: TextStyle(
                              fontFamily: 'inter',
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff228BDB)),
                        ),
                        Text(
                          "TASKS",
                          style: TextStyle(
                              fontFamily: 'inter',
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff8F8F8F)),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Card(
                    shadowColor: Color(0xff1D3149),
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0),
                    ),
                    child: Container(
                      height: 108.h,
                      width: 108.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24),
                          color: const Color(0x0fffffff)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text(
                            "150",
                            style: TextStyle(
                                fontFamily: 'inter',
                                fontSize: 20,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff228BDB)),
                          ),
                          Text(
                            "ORDERS",
                            style: TextStyle(
                                fontFamily: 'inter',
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff8F8F8F)),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  shadowColor: const Color(0xff1D3149),
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24.0),
                  ),
                  child: Container(
                    height: 108.h,
                    width: 108.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        color: const Color(0x0fffffff)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "20",
                          style: TextStyle(
                              fontFamily: 'inter',
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff228BDB)),
                        ),
                        Text(
                          "MESSAGES",
                          style: TextStyle(
                              fontFamily: 'inter',
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff8F8F8F)),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0,left: 24,right: 24),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: const [
                 Text(
                   "Sales Report",
                   style: TextStyle(
                       fontFamily: 'inter',
                       fontSize: 16,
                       fontWeight: FontWeight.w500,
                       color: Color(0xff403E3D)),
                 ),
                 Text(
                   "Feb - Mar 2022",
                   style: TextStyle(
                       fontFamily: 'inter',
                       fontSize: 14,
                       fontWeight: FontWeight.w400,
                       color: Color(0xff737372)),
                 ),
               ],
            ),
          ),
          SizedBox(
              width: 400.w,
              height: 278.h,
              child: SvgPicture.asset("assets/images/Sales Report.svg",fit: BoxFit.cover,))
        ],
      ),
    );
  }
}
