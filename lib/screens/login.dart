import 'package:engaged/models/login_model.dart';
import 'package:engaged/progress.dart';
import 'package:engaged/screens/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

import '../services/api.dart';
import 'home.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final myNameController = TextEditingController();
  final myPasswordController = TextEditingController();

  final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalformkey = GlobalKey<FormState>();
  LoginRequest? request;
  bool isApiCallProcess = false;
  Position? _currentPosition;
  String? _currentAddress;

  bool ValidateAndSave() {
    final form = globalformkey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  _getCurrentLocation() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        _getAddressFromLatLng();
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentPosition!.latitude, _currentPosition!.longitude);

      Placemark place = placemarks[0];

      setState(() {
        _currentAddress =
            "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Progress(
      child: _uiSetup(context),
      inAsyncCall: isApiCallProcess,
      opacity: 0.3,
    );
  }

  Widget _uiSetup(BuildContext context) {
    ScreenUtil.init(
        BoxConstraints(
            minWidth: 0.0,
            minHeight: 0.0,
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(MediaQuery.of(context).size.width,
            MediaQuery.of(context).size.height),
        context: context,
        minTextAdapt: true,
        orientation: Orientation.portrait);
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: const Color(0xffFBF9F7),
        body: SafeArea(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 64, top: 80),
                  child: SizedBox(
                      height: 48.h,
                      width: 282.w,
                      child: Image.asset("assets/images/Logo.png")),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 100),
                  child: Text(
                    "Engage your client | staff | customer",
                    style: TextStyle(
                        color: Color(0xff737372),
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                        fontStyle: FontStyle.normal,
                        fontFamily: 'inter'),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 78.0, right: 55, bottom: 80),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.grey),
                            child: Center(
                              child:
                                  SvgPicture.asset("assets/images/Group21.svg"),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 4.0),
                            child: Text(
                              "In-Field",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'inter'),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.grey),
                            child: Center(
                              child: SvgPicture.asset(
                                  "assets/images/Group 225.svg"),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 4.0),
                            child: Text(
                              "Recruiter ",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'inter'),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.grey),
                            child: Center(
                              child:
                                  SvgPicture.asset("assets/images/Group12.svg"),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 4.0),
                            child: Text(
                              "e-learning",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'inter'),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 16, right: 313, bottom: 16),
                  child: Text("Login",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff403E3D),
                          fontFamily: 'inter')),
                ),
                Form(
                  key: globalformkey,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(
                                left: 16.0,
                                right: 285,
                              ),
                              child: Text("USER NAME",
                                  style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff737372),
                                    fontFamily: 'inter',
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(16.0, 8, 16, 16),
                              child: TextFormField(
                                controller: myNameController,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter your username';
                                  }
                                },
                                keyboardType: TextInputType.name,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: const BorderSide(
                                        color: Color(0xff2BAEB2),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: const BorderSide(
                                        color: Color(0xff757575),
                                      ),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    filled: true,
                                    hintStyle:
                                        TextStyle(color: Colors.grey[800]),
                                    fillColor: const Color(0xffFFFFFF)),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(
                                left: 16.0,
                                right: 285,
                              ),
                              child: Text("PASSWORD",
                                  style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff737372),
                                    fontFamily: 'inter',
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(16.0, 8, 16, 24),
                              child: TextFormField(
                                controller: myPasswordController,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {


                                    return 'Please enter the password';
                                  }
                                  else if (value.length < 6) {
                                    return 'password must be 6 letter';
                                  }
                                  else{
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: const BorderSide(
                                        color: Color(0xff2BAEB2),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: const BorderSide(
                                        color: Color(0xff757575),
                                      ),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    filled: true,
                                    fillColor: const Color(0xffFFFFFF)),
                              ),
                            ),
                          ],
                        ),
                      ]),
                ),
                InkWell(
                  onTap: () {
                    _getCurrentLocation();
                    if (globalformkey.currentState!.validate()) {

                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const HomePage(),
                          ),
                          (route) => false);
                    }
                    /*if (ValidateAndSave()) {
                      setState(() {
                        isApiCallProcess = true;
                      });
                      Api api = Api();
                      api.login(request!).then((value) {
                        setState(() {
                          isApiCallProcess = false;
                        });
                        if (value.token!.isNotEmpty) {
                          _getCurrentLocation();*/

                    //     } else {
                    //       final snackBar =
                    //           SnackBar(content: Text(value.error.toString()));
                    //       ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    //     }
                    //   });
                    // }
                  },
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(
                      16,
                      12,
                      16,
                      32,
                    ),
                    child: Container(
                      height: 48.h,
                      width: 328.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: const Color(0xff37B257)),
                      child: const Center(
                        child: Text(
                          "Login",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Color(0xffFFFFFF),
                              fontFamily: 'inter'),
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 40.4, left: 16, bottom: 56),
                      child: SizedBox(
                        height: 17.h,
                        width: 119.w,
                        child: const Text(
                          "Forgot Password?",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff338B4B),
                              fontFamily: 'inter'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 40.5, right: 16, bottom: 56),
                      child: SizedBox(
                        height: 17.h,
                        width: 124.w,
                        child:  InkWell(
                          child: const Text(
                            "Create an Account",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff403E3D),
                                fontFamily: 'inter'),
                          ),
                          onTap:() {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const RegisterPage()));

                          }),
                      ),
                    ),
                  ],
                ),
              ]),
            ),
          ),
        ));
  }
}
