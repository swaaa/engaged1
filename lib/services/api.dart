import 'dart:convert';

import 'package:engaged/models/login_model.dart';
import 'package:http/http.dart' as http;

class Api {
  Future<LoginResponse> login(LoginRequest loginRequest) async {
    String baseUrl = "https://reqres.in/api/login";
    final response =
        await http.post(Uri.parse(baseUrl), body: loginRequest.toJson());
    if (response.statusCode == 200 || response.statusCode == 400) {
      return LoginResponse.fromJson(json.decode(response.body));

    } else {
      throw Exception("failed to load data");
    }
  }
}
