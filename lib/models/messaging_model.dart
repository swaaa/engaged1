class MessagingModel {
  int? iD;
  String? message;
  int? employeeID;
  bool? isActive;
  String? dateModified;
  String? dateCreated;
  String? name;

  MessagingModel(
      {this.iD,
        this.message,
        this.employeeID,
        this.isActive,
        this.dateModified,
        this.dateCreated,
        this.name});

  MessagingModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    message = json['Message'];
    employeeID = json['EmployeeID'];
    isActive = json['IsActive'];
    dateModified = json['DateModified'];
    dateCreated = json['DateCreated'];
    name = json['Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['Message'] = this.message;
    data['EmployeeID'] = this.employeeID;
    data['IsActive'] = this.isActive;
    data['DateModified'] = this.dateModified;
    data['DateCreated'] = this.dateCreated;
    data['Name'] = this.name;
    return data;
  }
}