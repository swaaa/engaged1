class RegisterRequest {
  String? email;
  String? password;

  RegisterRequest({this.email, this.password});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "email": email!.trim(),
      "password": password!.trim(),
    };
    return map;
  }
}
class RegisterResponse {
  final String? token;
  final int? id;

  RegisterResponse({this.token, this.id});

  factory RegisterResponse.fromJson(Map<String, dynamic> json) {
    return RegisterResponse(
        token: json["token"] ?? "",
        id: json["id"] ?? "");
  }
}